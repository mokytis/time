var fonts = [
    "'Archivo', sans-serif",
    "'Noto Sans', sans-serif",
    "'Open Sans Condensed', sans-serif",
    "'Sunflower', sans-serif",
    "'Yantramanav', sans-serif",
    "'Economica', sans-serif",
    "'Boogaloo', cursive",
]

function changeFont() {
    let font = get("select").value;
    Cookies.set('font', font, { expires: 7 });
    document.body.style.fontFamily = font;
}

function changeFontColor() {
    let color = get("foregroundcolor").value;
    Cookies.set('fgc', color, { expires: 7 });
    setCSSVar('--font-color', "#"+color);
}

function changeBackgroundColor() {
    let color = get("backgroundcolor").value;
    Cookies.set('bgc', color, { expires: 7 });
    setCSSVar('--background-color', "#"+color);
    document.body.style.backgroundColor = color;
}

function changeFontSize() {
    let fontsize = get("myRange").value;
    Cookies.set('fntsz', fontsize, { expires: 7 });
    get("time").style.fontSize = fontsize + "vw";
}

function setValuesFromCookies() {
    let fgc = Cookies.get('fgc');
    let bgc = Cookies.get('bgc');
    let font = Cookies.get('font');
    let fntsz = Cookies.get('fntsz');
    if (fgc) {
        get("foregroundcolor").value = fgc;
    }
    if (bgc) {
        get("backgroundcolor").value = bgc;
    }
    if (font) {
        get("select").value = font;
    }
    if (fntsz) {
        get("myRange").value = fntsz;
    }
}

function createFontOptions() {
    for (let i in fonts) {
        let font = fonts[i];
        let option = document.createElement("option");
        option.value = font;
        option.innerHTML = getFontName(font);
        get("select").appendChild(option);
    }
}
