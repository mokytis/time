function get(id) {
    return document.getElementById(id);
}

function getFontName(string) {
    let name = string.split(",")[0];
    return name.substring(1, name.length-1);
}

function formatNum(num, a) {
  let b = 1;
    if (a) {
      b = a;
    }
    let t = num.toString();
    let prefix = t.length == b ? "0": "";
    return prefix + t;
}

function formatTime(time, format) {
    rules = {
      "%H": formatNum(time.hours),
      "%M": formatNum(time.minutes % 60),
      "%S": formatNum(time.seconds % 60),
      "%tM":formatNum(time.minutes),
      "%tS":formatNum(time.seconds),
    }
    if (time.unix != undefined) {
      rules["%U"] = time.unix.toString().substring(0, 10);
    }
    if (time.hund != undefined) {
      rules["%h"] = formatNum(time.hund % 100);
      rules["%th"] = formatNum(time.hund);
    }

    let formated = format;
    keys = Object.keys(rules);
    for (let i in keys) {
      rule = keys[i];
      formated = formated.replace(rule, rules[rule]);
    }
    return formated;
}

function hide(elm) {
  get(elm).style.display = 'none';
}

function show(elm) {
  get(elm).style.display = 'inline';
}

function date_to_time(date) {
  return {seconds: date.getSeconds(), minutes: date.getMinutes(), hours: date.getHours(), unix: date.getTime()}
}

function setRunGap(func, time){
  func();
  setInterval(func, time);
}

function setCSSVar(variable, value) {
  document.documentElement.style.setProperty(variable, value);
}

function showMenu() {
  get("mainmenu").checked = true;
}
